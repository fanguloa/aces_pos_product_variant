# -*- coding: utf-8 -*-
#################################################################################
# Author      : Acespritech Solutions Pvt. Ltd. (<www.acespritech.com>)
# Copyright(c): 2012-Present Acespritech Solutions Pvt. Ltd.
# All Rights Reserved.
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#################################################################################
{
    'name': 'POS Product Variant',
    'summary': 'POS - Product Variant',
    'version': '1.0',
    'category': 'Point Of Sale',
    'description': """
In POS we can use products variants with it's attributes and we can filter product by products attributes.
""",
    'author': 'Acespritech Solutions Pvt. Ltd.',
    'website': 'http://www.acespritech.com',
    'price': 25, 
    'currency': 'EUR',
    'version': '1.0.1',
    'depends': ['base', 'point_of_sale','sale'],
    'images': ['static/description/main_screenshot.png'],
    'data': [
        'view/aces_pos_product_variant.xml',
    ],
    'qweb': [
        'static/src/xml/pos.xml',
    ],
    'installable': True,
    'auto_install': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: